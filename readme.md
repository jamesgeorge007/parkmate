# ParkMate

A bare minimum parking lot registration system CLIfied.

## Usage

```sh
npx parkmate
```

It makes use of a `slots.json` file where all the data is being stored.

## Instructions

- Clone the repository to your local machine.
- Navigate to the project directory and install dependencies with `npm install`.
- Now, type in `npm link` which creates a symlink.
- `parkmate` can now be accessed globally.

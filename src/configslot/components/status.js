const React = require('react')
const { existsSync, readFileSync, writeFileSync } = require('fs')
const { Box, Color }= require('ink')
const Table = require('cli-table3')

let slots
if (existsSync('./slots.json')) {
  slots = JSON.parse(readFileSync('./slots.json'));
}

const tableView = new Table({
    head: ['Slot', 'Vehicle-ID']
})

const ViewStatus = () => {
  const allocatedSlots = slots.filter(slot => slot.vehicleId !== '')
  allocatedSlots.forEach(slot => {
    let obj = {}
    const [key, val] = Object.values(slot)
    obj[key] = val
    tableView.push(obj)
  })

  let msg = ` Allocated slots: ${allocatedSlots.length}\n Available slots: ${slots.length - allocatedSlots.length}`
  if (allocatedSlots.length !== 0) {
    msg += `\n${tableView.toString()}`
  }

  return (
    <Box>
        {
          console.log(msg)
        }
    </Box>
  )
}

module.exports = ViewStatus

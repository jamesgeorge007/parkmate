const React = require('react')
const { Box } = require('ink')

const Greet = () => {
  return (
    <Box marginBottom={1}>
      {
        console.log('Choose from below:-')
      }
    </Box>
  );
};

module.exports = Greet;

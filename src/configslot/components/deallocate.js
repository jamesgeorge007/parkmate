const React = require('react')
const inquirer = require('inquirer')
const { existsSync, readFileSync, writeFileSync } = require('fs')

let slots
if (existsSync('./slots.json')) {
  slots = JSON.parse(readFileSync('./slots.json'));
}

const DeAllocateSlot = () => {
  <React.Fragment>
    {
      inquirer.prompt({
        message: 'Enter a slot (A1-Z30):- ',
        name: 'slot',
        type: 'input',
        validate: slot => !slot ? 'kindly enter a slot' : true
      })
      .then(answer => {
        let idx = -1

        slots.forEach(slot => {
          if (slot.name === answer.slot) {
            if (slot.vehicleId === '') {
              console.log('Slot is already free')
              process.exit(1)
            } else {
              slot.vehicleId = ''
            }
            idx = slots.indexOf(slot)
            return
          }
        })

        if (idx !== -1) {
          writeFileSync('./slots.json', JSON.stringify(slots))
          console.log('Done')
        } else {
          console.log('Invalid slot')
        }
      })
    }
  </React.Fragment>
}

module.exports = DeAllocateSlot

const React = require('react')
const inquirer = require('inquirer')
const { existsSync, readFileSync } = require('fs')

let slots
if (existsSync('./slots.json')) {
  slots = JSON.parse(readFileSync('./slots.json'));
}

const Search = () => {
  <React.Fragment>
    {
      inquirer.prompt({
        message: 'Enter the vehicle ID:- ',
        name: 'vehicleId',
        type: 'input',
        validate: vehicleId => !vehicleId ? 'kindly enter a vehicle ID' : true
      })
      .then(answer => {
        let idx = -1
        slots.forEach(slot => {
          if (slot.vehicleId === answer.vehicleId) {
            idx = slots.indexOf(slot)
            return
          }
        })

        if (idx !== -1) {
          console.log(` Slot ${slots[idx].name} allocated to Vehicle ID:- ${answer.vehicleId}`)
        } else {
          console.log('Not allocated yet')
        }
      })
    }
  </React.Fragment>
}

module.exports = Search

const React = require('react')
const inquirer = require('inquirer')
const { existsSync, readFileSync, writeFileSync } = require('fs')

let slots
if (existsSync('./slots.json')) {
  slots = JSON.parse(readFileSync('./slots.json'));
}

const AllocateSlot = () => {
  <React.Fragment>
    {
      inquirer.prompt({
        message: 'Enter a slot (A1-Z30):- ',
        name: 'slot',
        type: 'input',
        validate: slot => !slot ? 'kindly enter a slot' : true
      })
      .then(answer => {
        const idx = slots.findIndex(slot => slot.name === answer.slot)

        const duplicatedSlotEntry = slots.filter(slot => slot.vehicleId !== '').some(slot => slot.name === answer.slot)
        if (duplicatedSlotEntry) {
          console.log(`\n ${answer.slot} is already allocated to vehicle ID: ${slots[idx].vehicleId}`)
          process.exit(1)
        }

        if (idx !== -1) {
          inquirer.prompt({
            message: 'Enter the vehicle-ID:- ',
            name: 'vehicleId',
            type: 'input',
            validate: vehicleId => !vehicleId ? 'invalid input' : true
          })
          .then(answer => {
            const duplicateIdIndex = slots.findIndex(slot => slot.vehicleId === answer.vehicleId)
            if (duplicateIdIndex !== -1) {
              console.log(`Slot ${slots[duplicateIdIndex].name} is already allocated to ${answer.vehicleId}`)
            } else {
              slots[idx].vehicleId = answer.vehicleId
              writeFileSync('./slots.json', JSON.stringify(slots))
              console.log('Done')
            }
          })
        } else {
          console.log('\n Invalid slot')
        }
      })
    }
  </React.Fragment>
}

module.exports = AllocateSlot

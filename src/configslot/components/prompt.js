const React = require('react')
const { Box, Color } = require('ink')
const SelectBox = require('ink-select-input').default
const importJsx = require('import-jsx')

const AllocateSlot = importJsx('./allocate')
const DeAllocateSlot = importJsx('./deallocate')
const Search = importJsx('./search')
const ViewStatus = importJsx('./status')

const Prompt = () => {
  const [optionNotChosen, setFlag] = React.useState(true)
  const [choice, setChoice] = React.useState('')

  const handleSelect = item => {
    console.log(item)
    setFlag(false)
    setChoice(item.value)
  };

  const items = [
    {
      label: 'Allocate slot',
      value: AllocateSlot
    },
    {
      label: 'Deallocate slot',
      value: DeAllocateSlot
    },
    {
      label: 'Search for a vehicle',
      value: Search
    },
    {
      label: 'View allocation status',
      value: ViewStatus
    }
  ]

  let selectBox, component

  if (optionNotChosen) {
    selectBox = <SelectBox items={items} onSelect={handleSelect.bind(this)}></SelectBox>
    component = ''
  } else {
    selectBox = ''
    component = choice
  }

  return (
    <Box>
      {selectBox}
      {component}
    </Box>
  );
};

module.exports = Prompt;

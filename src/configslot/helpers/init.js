const { writeFileSync } = require('fs')

const initializeSlots = () => {
  const slots = []

  for (let slot = 65; slot <= 90; slot++) {
    for (let sub = 1; sub <= 30; sub++) {
      slots.push({
        name: String.fromCharCode(slot) + sub,
        vehicleId: ''
      });
    }
  }

 writeFileSync('./slots.json', JSON.stringify(slots))
};

module.exports = initializeSlots

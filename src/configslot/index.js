'use strict'

const React = require('react')
const importJsx = require('import-jsx')
const { Box } = require('ink')

const Greet = importJsx('./components/greet')
const Prompt = importJsx('./components/prompt')

const ConfigSlot = () => {
  return (
    <Box>
      <Greet />
      <Prompt />
    </Box>
  )
}

module.exports = ConfigSlot
